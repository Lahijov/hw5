package com.company;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    Human father;
    Human[] children = new Human[0];
    Pet pet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.deepToString(children) +
                ", pet=" + pet +
                '}';
    }

    public Family(Human mother, Human father, Human[] children) {
        this.mother = mother;
        this.father = father;
        this.children = children;
    }

    public void addChild(Human human) {
        int i;

        int n = children.length;

        Human newChild[] = new Human[n + 1];
        for (i = 0; i < n; i++)
            newChild[i] = children[i];
        this.children = newChild;
    }


    public boolean deleteChild(int index) {
        boolean deleted = false;

        Human[] child = new Human[children.length - 1];
        int i = 0;
//        for (int j = 0, k = 0; i < children.length; i++) {
//            if (i == index) {
//                continue;
//            }
//            deleted = true;
//
//            child[k++] = children[i];
//
//        }
//        return deleted;
//    }
        try {
            for (int j = 0; j < children.length; j++) {

                if (j != index) {
                    child[i++] = children[j];
                }
            }
            this.children = child;
            if (index < child.length - 1) {
                deleted = true;
            }
            return deleted;
        } catch (
                Exception exception) {
            System.out.println("error");
        }
        return false;
    }

    public int countFamily() {
        return 2 + children.length;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}

