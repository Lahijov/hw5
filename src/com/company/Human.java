package com.company;

import java.util.Arrays;

public class Human {
    String name;
    String surname;
    int year;
    int iq;
    Family family;
    String[][] scedule = new String[7][2];


    public void welcome() {
        System.out.println("Hello," + family.getPet().nickname);
    }

    public void favouritePet() {
        System.out.print("I have a " + family.getPet().species + "," + "he is " + family.getPet().age +
                " years old,he is ");
        if (iq < 50) {
            System.out.println("almost not sly");
        } else if (iq > 50) {
            System.out.println("very sly");
        }
    }
    public void feed () {

    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(scedule)+
                '}';
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human() {
    }
    public Human(String name, String surname, int year, int iq , String[][] scedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if(iq<=100 || iq>=1){
            this.iq = iq;}
        else{
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq=99;
        }

        this.scedule = scedule;
    }
}

